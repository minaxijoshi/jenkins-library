package com.seh.utils;

def call(Map jiraParams) {
   def projectkey= jiraParams.appName[0]+""+jiraParams.appName[1]+""+jiraParams.appName[2]
   withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId:'svc_devops_jenkins_token',
  usernameVariable: 'USER', passwordVariable: 'PASSWORD']
]) {
     sh """
     echo $USER
     echo $PASSWORD >> pass.txt
     cat pass.txt
     curl -u ${USER}:${PASSWORD} -X GET 'http://localhost:8082/rest/scriptrunner/latest/custom/newClonedProject?project_key=${projectkey}&project_name=${jiraParams.appName}'
     """
     }
}