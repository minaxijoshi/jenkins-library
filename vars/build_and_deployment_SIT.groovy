import com.fwd.utils.loginArtifactManagement
import com.fwd.utils.artifactInfo
@Library("jenkins-shared-library")_
import com.fwd.utils.*
    try {
        timeout(time: 60, unit: 'MINUTES') {
            pipeline {
                Map appParams= [:]
                Map buildParams= [:]
                //set ENV_VAR=sit (from jenkins)
                env.ENV_VAR=params.ENV
                appParams.put("JIRA_KEY", params.JIRA_KEY)
                buildParams.put("JIRA_KEY", params.JIRA_KEY)
                node(params.JENKINS_NODE) {
                    stage("Build Provisioning") {
                        new readJIRA().call(buildParams,appParams)
                        new checkoutSCM().call(appParams)
                        new buildSpecReader().call(buildParams,appParams)
                        new artifactInfo().call(buildParams,appParams)
                        new buildCompileApp().call(buildParams)
                    }
                    stage("Static Code Analysis") {
                        new staticCodeAnalysis().call(appParams,buildParams)
                        build job: "../../coverity_scan", parameters: [string(name: 'JIRA_KEY', value: params.JIRA_KEY)], wait: false
                    }
                    stage("Upload to Artifact Repo") {
                        new loginArtifactManagement().call(buildParams)
                        new uploadArtifacts().call(buildParams)
                    }
                    stage("Deploy") {
                        buildParams.put("RETRO",false)
                        new uploadSnapshotArtifacts().call(buildParams)
                        new deployApp().call(buildParams)  
                        def transitionInput = [transition: [id: '131']]
                        def response = ""
                        try{
                            response =jiraTransitionIssue idOrKey: params.JIRA_KEY, input: transitionInput, site: 'JIRA_SG'
                        }catch(errr) {
                            response =jiraTransitionIssue idOrKey: params.JIRA_KEY, input: transitionInput, site: 'JIRA_SG'
                        }
                        
                        messaging = "Deployment Successful in ${ENV_VAR}"
                        jiraAddComment comment: messaging, idOrKey: params.JIRA_KEY, site: 'JIRA_SG'
                        print response
                    }
                }
            }
        }
    }catch (err) {
        echo "Caught: ${err}"
        def transitionInput = [transition: [id: '121']]
        try{
            jiraTransitionIssue idOrKey: params.JIRA_KEY, input: transitionInput, site: 'JIRA_SG'
        }catch(errr) {
            jiraTransitionIssue idOrKey: params.JIRA_KEY, input: transitionInput, site: 'JIRA_SG'
        }
        messaging = "Deployment Failed in ${ENV_VAR}"
        jiraAddComment comment: messaging, idOrKey: params.JIRA_KEY, site: 'JIRA_SG'
        throw err
    }finally{
        node(params.JENKINS_NODE) {
            withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId:'svc_devops_jenkins_token',usernameVariable: 'USER',passwordVariable: 'PASSWORD']]) {
	            sh """
	                curl -XGET "${BUILD_URL}/consoleText" --user ${USER}:${PASSWORD} -H "Content-Type:application/x-www-form-urlencoded" -o build.log
	            """
	        }
            try{
	            jiraUploadAttachment idOrKey: params.JIRA_KEY, file: 'build.log', site: 'JIRA_SG'
            }catch(errr) {
                jiraUploadAttachment idOrKey: params.JIRA_KEY, file: 'build.log', site: 'JIRA_SG'
            }
            cleanWs()
        }
    }