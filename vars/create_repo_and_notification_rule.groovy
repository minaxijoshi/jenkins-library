import com.seh.utils.autoCreateJiraShell
import com.seh.utils.autoCreateRepo
@Library("jenkins-shared-library")_
import com.seh.utils.*
    try {
        timeout(time: 60, unit: 'MINUTES') {
            pipeline {
                Map jiraParams= [:]
                def JIRA_KEY = params.JIRA_ISSUE_KEY
                node("master") {
                    //JIRA sites created in jenkins configuration
                    def issue = jiraGetIssue idOrKey: JIRA_KEY, site: 'JIRA_SG'
                    List<String> repoNames = Arrays.asList(issue.data.fields.customfield_10007.toString().split("\\s*,\\s*"));
                    //String projectName = issue.data.fields.customfield_13706.toString().replaceAll(' ','_')
                    String appName = issue.data.fields.customfield_10008.toString().replaceAll(' ','_')
                    String repoAdmin =  issue.data.fields.customfield_10009.toString().replaceAll(' ','_')
                    String exclusions = issue.data.fields.customfield_10011.toString()
                    jiraParams.put("appName", appName)
                    jiraParams.put("repoAdmin", repoAdmin)

                    stage("Create Repo & Jenkins Job") {
                        for(repo in repoNames) {
                            if(!exclusions.contains("Exclude GITHUB Repo"))
                            {
                                print "repo" +repo +"appName  "+appName
                                new autoCreateRepo().call(repo,appName,repoAdmin)
                                //new applyNotificationRule().call(repo)
                            }
                            if(!exclusions.contains("Exclude Jenkins Jobs"))
                            {
                               print "repo" +repo +"appName"+appName
                               new autoCreateJob().call(repo,appName)
                            }
                        }
                        if(!exclusions.contains("Exclude Jira Project Creation"))
                        {
                            print "jira creation"
                            new autoCreateJiraShell().call(jiraParams)
                        }
                    }
                    stage("Update Jira") {
                        echo "Update Jira"
                        def transitionInput = [transition: [id: '21']]
                        def response = jiraTransitionIssue idOrKey: params.JIRA_ISSUE_KEY, input: transitionInput, site: 'JIRA_SG'
                        print response
                    }  
                }
            }
        }
    }catch (err) {
        echo "Caught: ${err}"
        def transitionInput = [transition: [id: '31']]
        jiraTransitionIssue idOrKey: params.JIRA_ISSUE_KEY, input: transitionInput, site: 'JIRA_SG'
        messaging = 'DO ticket Failed'
        jiraAddComment comment: messaging, idOrKey: params.JIRA_ISSUE_KEY, site: 'JIRA_SG'
        throw err
    }finally {
         node("master") {
           sh "rm -rf ${WORKSPACE}/*"
        }
    }
